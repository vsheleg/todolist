import { Meteor } from "meteor/meteor";
import { Tasks } from "../";

function insertTask(text) {
  Tasks.insert({ task: text, isChecked: false, createdAt: new Date() });
}

Meteor.startup(function () {
  if (!Tasks.findOne()) {
    [
      "First Task",
      "Second Task",
      "Third Task",
      "Fourth Task",
      "Fifth Task",
      "Sixth Task",
      "Seventh Task",
    ].forEach(insertTask);
  }
});
