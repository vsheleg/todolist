import { Tasks } from "../";
import { Meteor } from "meteor/meteor";

Meteor.publish("tasks", function () {
  return Tasks.find({
    $or: [{ isPrivate: { $ne: true } }, { owner: this.userId }],
  });
});
