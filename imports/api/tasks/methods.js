import { Meteor } from "meteor/meteor";
import { Tasks } from "./";
import { ValidatedMethod } from "meteor/mdg:validated-method";

const insert = new ValidatedMethod({
  name: "tasks.insert",
  validate: new SimpleSchema({
    task: { type: String },
  }).validator({}),
  run({ task }) {
    /*const newTask = Tasks.insert(task);
    if (!newTask.editableBy(this.userId)) {
      throw new Meteor.Error('todos.updateText.unauthorized',
        'Cannot edit todos in a private list that is not yours');
    }*/
    Tasks.insert({
      task: task,
      createdAt: new Date(),
      owner: this.userId,
      username: Meteor.users.findOne(this.userId).username,
      isChecked: false,
    });
  },
});

const setChecked = new ValidatedMethod({
  name: "tasks.setChecked",
  validate: new SimpleSchema({
    isChecked: { type: Boolean },
    taskId: { type: String },
  }).validator({}),
  run({ isChecked, taskId }) {
    Tasks.update(taskId, {
      $set: {
        isChecked,
      },
    });
  },
});

const remove = new ValidatedMethod({
  name: "tasks.remove",
  validate: new SimpleSchema({
    taskId: { type: String },
  }).validator({}),
  run({ taskId }) {
    const task = Tasks.findOne(taskId);

    if (!this.userId || task.owner !== this.userId) {
      throw new Meteor.Error("Not authorized.");
    }

    Tasks.remove(taskId);
  },
});
const setPrivate = new ValidatedMethod({
  name: "tasks.setPrivate",
  validate: new SimpleSchema({
    taskId: { type: String },
    isPrivate: { type: Boolean },
  }).validator({}),
  run({ taskId, isPrivate }) {
    const task = Tasks.findOne(taskId);
    if (!this.userId || task.owner !== this.userId) {
      throw new Meteor.Error("Not authorized.");
    }

    Tasks.update(taskId, {
      $set: {
        isPrivate,
      },
    });
  },
});
