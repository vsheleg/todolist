import "./api";
import { Meteor } from "meteor/meteor";

Meteor.startup(() => {
  Meteor.methods({
    signup: function (data) {
      user = Accounts.createUser({
        username: data.username,
        email: data.email,
        password: data.password,
      });
      return {
        userId: user,
      };
    },
  });
});
