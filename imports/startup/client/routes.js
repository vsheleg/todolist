import "../../ui/pages/LoginForm";
import React from "react";
import { FlowRouter } from "meteor/kadira:flow-router";
import { LoginForm } from "../../ui/pages/LoginForm";
import { SignupForm } from "../../ui/pages/SignupForm";
import { mount } from "react-mounter";
import LoginLayout from "../../ui/layout/loginLayout";
import SignupLayout from "../../ui/layout/signupLayout";
import { App } from "../../ui/App";
import TasksLayout from "../../ui/layout/tasksLayout";
/*
FlowRouter.route("/login", {
  name: "Lists.show",
  action() {
    mount(App, {
      main: <LoginForm />,
    });
  },
});

FlowRouter.route("/login", {
  name: "Main.login",
  action(params, queryParams) {
    console.log("Looking at a list?");
    BlazeLayout.render("applicationLayout", {
      area: "blog",
    });
  },
});
*/
FlowRouter.route("/login", {
  name: "login",
  action() {
    mount(LoginLayout, {
      content: <LoginForm />,
    });
  },
});
FlowRouter.route("/signup", {
  name: "signup",
  action() {
    mount(SignupLayout, {
      content: <SignupForm />,
    });
  },
});
FlowRouter.route("/", {
  name: "app",
  action() {
    mount(TasksLayout, {
      content: <App />,
    });
  },
});
