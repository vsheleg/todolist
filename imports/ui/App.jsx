import React, { useState } from "react";
import { useTracker } from "meteor/react-meteor-data";
import { Task } from "./Task/Task";
import _ from "lodash";
import { Redirect } from "react-router-dom";
import { Meteor } from "meteor/meteor";
import { Tasks } from "../api/Tasks";
import { TaskForm } from "./AddTaskForm/AddTaskForm";
import { LoginForm } from "../ui/LoginForm/LoginForm";
import "./Task/Task.css";

const toggleChecked = ({ _id, isChecked }) => {
  //Tasks.update(_id, { $set: { isChecked: !isChecked } }); //$set replaces the value of a field
  Meteor.call("tasks.setChecked", _id, !isChecked);
};
const togglePrivate = ({ _id, isPrivate }) => {
  Meteor.call("tasks.setPrivate", _id, !isPrivate);
};

const deleteTask = ({ _id }) => Meteor.call("tasks.remove", _id);
export const App = () => {
  const [isHided, setIsHided] = useState(false);
  const [logout, setLogout] = useState(false);
  const filter = {};

  if (isHided) {
    _.set(filter, "isChecked", false);
    //filter.checked = false
  }

  const { tasks, incompleteTasksCount, user } = useTracker(() => {
    Meteor.subscribe("tasks");

    let elems = Tasks.find({ checked: { $ne: true } });
    console.log(elems);
    return {
      tasks: Tasks.find(filter, { sort: { createdAt: -1 } }).fetch(),
      incompleteTasksCount: Tasks.find({ isChecked: { $ne: true } }).count(),
      user: Meteor.user(),
    };
  });
  if (logout) {
    return <Redirect to="/login" from="/" />;
  }
  console.log(user);
  if (!user) {
    return (
      <div>
        <LoginForm />
      </div>
    );
  }
  return (
    <div id="task-container">
      <h1>Todo List ({incompleteTasksCount})</h1>

      <div className="filters">
        <label>
          <input
            type="checkbox"
            readOnly
            checked={Boolean(isHided)}
            onClick={() => setIsHided(!isHided)}
          />
          Hide Completed
        </label>
      </div>
      <ul>
        {tasks.map((task) => (
          <Task
            key={task._id}
            task={task}
            onCheckboxClick={toggleChecked}
            onDeleteClick={deleteTask}
            onTogglePrivateClick={togglePrivate}
          />
        ))}
      </ul>
      <TaskForm user={user} />
      <hr />
      <button
        id="logout"
        onClick={() => {
          Meteor.logout();
          setLogout(!logout);
        }}
      >
        Logout
      </button>
    </div>
  );
};
