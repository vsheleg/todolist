import React from "react";

export const Task = ({
  task,
  onCheckboxClick,
  onDeleteClick,
  onTogglePrivateClick,
}) => {
  return (
    <li>
      <button onClick={() => onDeleteClick(task)}>&times;</button>
      <button onClick={() => onTogglePrivateClick(task)}>
        {task.isPrivate ? "Private" : "Public"}
      </button>
      <input
        className="task"
        type="checkbox"
        checked={Boolean(task.isChecked)}
        onClick={() => onCheckboxClick(task)}
        readOnly
      />
      <span>{task.task}</span>
    </li>
  );
};
