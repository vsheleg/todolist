import React, { useState } from "react";
import { Tasks } from "../../../api/tasks/tasks";
import "./style.css";

export const TaskForm = ({ user }) => {
  const [text, setText] = useState("");

  const handleSubmit = () => {
    if (!text) return;
    Meteor.call("tasks.insert", { task: text });
    setText("");
  };

  return (
    <form className="task-form" onSubmit={handleSubmit}>
      <input
        className="add-task"
        type="text"
        value={text}
        onChange={(e) => setText(e.target.value)}
        placeholder="Type to add new tasks"
      />

      <button type="submit" className="add-task">
        Add Task
      </button>
    </form>
  );
};
