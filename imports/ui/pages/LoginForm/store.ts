//import { makeObservable, observable, action } from "mobx"
import { makeObservable, observable, computed, action } from "mobx";

export class Store {
  name = "";
  password = "";

  constructor() {
    makeObservable(this, {
      name: observable,
      password: observable,
      enabled: computed,
      setName: action,
      setPassword: action,
    });
  }
  get enabled() {
    return Boolean(this.name && this.password);
  }
  setName(name) {
    this.name = name;
  }
  setPassword(password) {
    this.password = password;
  }
}
