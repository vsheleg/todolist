import React, { useEffect, useState } from "react";
import {Store} from "./store";
import { observer } from "mobx-react-lite"
import "./style.css";


export const LoginForm = observer(({}) => {

  const [loginStore] = useState(() => new Store()) 


  Meteor.logout();
  const submit = (e) => {
    e.preventDefault();
   Meteor.loginWithPassword(loginStore.name, loginStore.password);
  };

 return (
    <div id="login-form">
      <form onSubmit={submit}>
        <p id="form-header">Login</p>
        <hr />
        <input
          required
          type="text"
          onChange = {(e)=> loginStore.setName(e.currentTarget.value)}
          value={loginStore.name} 

      
          placeholder="Username"
          name="username"
          className="login"
        />
        <br />
        <input
          required
          type="password"
          placeholder="Password"
          onChange = {(e)=> loginStore.setPassword(e.currentTarget.value)}
          value={loginStore.password} 
   
     
          name="password"
          className="login"
        />
        <br />
        <button type="submit" id="login-btn" disabled={!Boolean(loginStore.enabled)}>
          Log In
        </button>
        <br />
        <span>Don't have an account? </span>
        <a href="/signup"> Sign up here</a>
      </form>
    </div>
  );
})
