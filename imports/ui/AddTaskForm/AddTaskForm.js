import React, { useState } from "react";
import { Tasks } from "../../api/Tasks";
import "./AddTaskForm.css";

export const TaskForm = ({ user }) => {
  const [text, setText] = useState("");

  const handleSubmit = () => {
    if (!text) return;
    Meteor.call("tasks.insert", text);
    /*
    Tasks.insert({
      task: text.trim(),
      createdAt: new Date(),
      owner: user._id,
    });*/

    setText("");
  };

  return (
    <form className="task-form" onSubmit={handleSubmit}>
      <input
        className="add-task"
        type="text"
        value={text}
        onChange={(e) => setText(e.target.value)}
        placeholder="Type to add new tasks"
      />

      <button type="submit" className="add-task">
        Add Task
      </button>
    </form>
  );
};
