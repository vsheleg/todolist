import React, { useState } from "react";
import { Redirect } from "react-router-dom";

import "./SignupForm.css";
//Meteor.user is stored in local storage
//why lodash
//drop & remove
//redirect doesn't cause rerender
export const SignupForm = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [redirect, setRedirect] = useState(false);
  Meteor.logout();
  const submit = (e) => {
    e.preventDefault();
    /*  Users.insert({
      username: username,
      password: password,
      email: email,
    });
    Accounts.createUser(
      { username: username, password: password, email: email },
      function (err) {
        if (err) console.log(err);
        else console.log("success!");
      }
    );*/
    Meteor.call(
      "signup",
      { email: email, password: password, username: username },
      function (error, result) {
        if (result) {
          console.log(result);
          setRedirect(result);
        }
        if (error) {
          console.log(error);
        }
      }
    );
  };
  if (redirect) {
    return <Redirect to="/login" from="/signup" />;
  }
  return (
    <div id="registration-form">
      <form onSubmit={submit}>
        <p id="form-header">Login</p>
        <hr />
        <input
          className="signup"
          required
          type="text"
          onChange={(e) => setUsername(e.currentTarget.value)}
          placeholder="Username"
          name="username"
        />
        <br />
        <input
          required
          type="email"
          placeholder="Email"
          className="signup"
          onChange={(e) => setEmail(e.currentTarget.value)}
          name="email"
        />
        <br />
        <input
          required
          className="signup"
          type="password"
          placeholder="Password"
          onChange={(e) => setPassword(e.currentTarget.value)}
          name="password"
        />
        <br />

        <button type="submit">Sign up</button>
        <br />
        <span>Already have an account? </span>
        <a href="/login"> Login here</a>
      </form>
    </div>
  );
};
