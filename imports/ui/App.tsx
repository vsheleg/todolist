import React, { useState } from "react";
import { useTracker } from "meteor/react-meteor-data";
import { Task } from "./pages/Task";
import "../startup/client";
import { Meteor } from "meteor/meteor";

import { Tasks } from "../api/tasks";
import { TaskForm } from "./pages/AddTaskForm";
import "../api/tasks/methods";

const container = {
  backgroundColor: "#6b6e70",
  borderStyle: "solid",
  borderWidth: "2px",
  borderColor: "#6b6e70",
  padding: "20px",
  color: "white",
  borderRadius: "2%",
};
const line = {
  backgroundColor: "#86c232",
  height: "2px",
  border: "none",
};
const task = {
  width: "350px",
  height: "30px",
  fontSize: "17px",
  color: "#222629",
  padding: "10px",
  margin: "10px",
};
const toggleChecked = ({ _id, isChecked }) => {
  //Tasks.update(_id, { $set: { isChecked: !isChecked } }); //$set replaces the value of a field
  Meteor.call("tasks.setChecked", { taskId: _id, isChecked: !isChecked });
};
const togglePrivate = ({ _id, isPrivate }) => {
  Meteor.call("tasks.setPrivate", { taskId: _id, isPrivate: !isPrivate });
};

const deleteTask = ({ _id }) => Meteor.call("tasks.remove", { taskId: _id });
export const App = () => {
  const [isHided, setIsHided] = useState(false);

  const filter = {};

  if (isHided) {
    filter["isChecked"] = false;
  }

  const { tasks, incompleteTasksCount, user } = useTracker(() => {
    Meteor.subscribe("tasks");

    return {
      tasks: Tasks.find(filter, { sort: { createdAt: -1 } }).fetch(),
      incompleteTasksCount: Tasks.find({ isChecked: { $ne: true } }).count(),
      user: Meteor.user(),
    };
  });

  /* if (!user) {
    return (
      <div>
        <LoginForm />
      </div>
    );
  }*/
  return (
    <div id="task-container" style={container}>
      <h1>Todo List ({incompleteTasksCount})</h1>

      <div className="filters">
        <label>
          <input
            type="checkbox"
            readOnly
            checked={Boolean(isHided)}
            onClick={() => setIsHided(!isHided)}
          />
          Hide Completed
        </label>
      </div>
      <ul>
        {tasks.map((task) => (
          <Task
            key={task._id}
            task={task}
            onCheckboxClick={toggleChecked}
            onDeleteClick={deleteTask}
            onTogglePrivateClick={togglePrivate}
          />
        ))}
      </ul>
      <TaskForm user={user} />
      <hr style={line} />
      <button
        id="logout"
        onClick={() => {
          Meteor.logout();
        }}
      >
        Logout
      </button>
    </div>
  );
};
