/*import React from "react";
import { Router, Route, Switch } from "react-router";
import { createBrowserHistory } from "history";

// route components
import { LoginForm } from "./LoginForm/LoginForm";
import { App } from "./App";
import { render } from "react-dom";

const browserHistory = createBrowserHistory();

export const renderRoutes = () => (
  <Router history={browserHistory}>
    <Switch>
      <Route path="/" component={App} />
    </Switch>
  </Router>
);

Meteor.startup(() => {
  render(renderRoutes(), document.getElementById("react-target"));
});

//<Route exact path="/login" component={LoginForm} />
*/
import React from "react";
import { Router, Route, Switch } from "react-router";
import { createBrowserHistory } from "history";

// route components
import { LoginForm } from "./LoginForm/LoginForm";
import { App } from "./App";
import { SignupForm } from "./SignupForm/SignupForm";

const browserHistory = createBrowserHistory();

export const renderRoutes = () => (
  <Router history={browserHistory}>
    <Switch>
      <Route exact path="/login" component={LoginForm} />
      <Route exact path="/signup" component={SignupForm} />
      <Route path="/" component={App} />
    </Switch>
  </Router>
);
