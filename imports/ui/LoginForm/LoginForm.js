import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import "./LoginForm.css";
import { set } from "lodash";

export const LoginForm = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [redirect, setRedirect] = useState(false);
  //Meteor.logout();
  const submit = (e) => {
    e.preventDefault();

    Meteor.loginWithPassword(username, password);
    //loginWithPassword() callback doesn't have arguments on success.
    setRedirect(true);
  };

  if (redirect) {
    return <Redirect to="/" from="/login" />;
  }
  return (
    <div id="login-form">
      <form onSubmit={submit}>
        <p id="form-header">Login</p>
        <hr />
        <input
          required
          type="text"
          onChange={(e) => setUsername(e.currentTarget.value)}
          placeholder="Username"
          name="username"
          className="login"
        />
        <br />
        <input
          required
          type="password"
          placeholder="Password"
          onChange={(e) => setPassword(e.currentTarget.value)}
          name="password"
          className="login"
        />
        <br />
        <button type="submit">Log In</button>
        <br />
        <span>Don't have an account? </span>
        <a href="/signup"> Sign up here</a>
      </form>
    </div>
  );
};
